# X deb

A private deb repository.

This repository provides the latest Debian packages:

- adguard-home
- bat
- cargo-deb
- clash, clash-geoip
- codium
- curl
- dishus
- fd
- gogs
- golang
- goproxy
- hexyl
- htop
- hyperfine
- keepassxc
- mdbook
- nfpm
- shadowsocks-libev
- shadowsocks-rust
- tmux-mem-cpu-load
- v2ray
- v2ray-plugin

## Usage

1. Set up the APT Key

   Add the repository's APT Key by following command:

        wget -O /etc/apt/trusted.gpg.d/xdeb-gitlab-io.gpg https://xdeb.gitlab.io/debian/pubkey.gpg

   or (if you prefer text formatted key)

        wget -O /etc/apt/trusted.gpg.d/xdeb-gitlab-io.asc https://xdeb.gitlab.io/debian/pubkey.asc

2. Set up `sources.list`

   Create a new source list file at `/etc/apt/sources.list.d/xdeb-gitlab-io.list` with the
   content as following (debian/bookworm for example):

        deb https://xdeb.gitlab.io/debian bookworm contrib

   The available source list files are:

   * Debian 11 (bookworm)

         deb https://xdeb.gitlab.io/debian bullseye contrib

   * Debian 12 (bookworm)

         deb https://xdeb.gitlab.io/debian bookworm contrib
